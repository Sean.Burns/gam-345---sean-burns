// GAM345LinkedList.h
// This file contains the declarations and defenitions of functions in a LinkedList class.
// Sean Burns

#pragma once

namespace GAM345
{

	template <class T>

	class LinkedList
	{

	private:
		class node // Node class in a LinkedList.
		{
		public: 
			T* data; // Pointer to data within a node;
			node* next; // Pointer to the next node in the list. 
		};
		node* head; // Pointer to the first node in the list.
		node* tail; // Pointer to the last node in the list.
		node* curr; // Pointer to the current node in the list.
		node* prev; // Pointer used to change previous nodes' pointers.
		int sizeOfList; // The number of nodes in the list.

	public:
		LinkedList() // Constructor for a linked list.
		{
			head = NULL; 
			tail = NULL;
			sizeOfList = 0;
		}

		void insert(T* newElement, int position) // insert a node in a specific location in a list
		{
			if (position < 0 || position > sizeOfList) // invalid position, will not insert a node
			{
				return;
			}
			else
			{
				node* n = new node;
				n->data = reinterpret_cast<T*>(new char[sizeof(T)]);
				n->data = newElement;
				n->next = NULL;

				if (sizeOfList == 0 && position == 0) // creating the first node in list
				{
					head = n;
					tail = n;
				}
				else if (sizeOfList > 0 && position == 0) // creating a node in the beggining of a list
				{
					n->next = head;
					head = n;
				}
				else if (sizeOfList > 0 && position < sizeOfList) // creating a node in the middle of a list
				{
					curr = head;
					prev = NULL;

					for (int i = 0; i < position; i++)
					{
						prev = curr;
						curr = curr->next;
					}

					n->next = curr;
					prev->next = n;
				}
				else if (sizeOfList > 0 && position == sizeOfList) // creating the last node in a list
				{
					prev = tail;
					n->next = NULL;
					prev->next = n;
					tail = n;
				}

				sizeOfList += 1;
			}
		}

		void erase(int position) // erase a specific node from a list
		{
			if (position < 0 || position > sizeOfList) // invalid position, will not erase a node
			{
				return;
			}
			else
			{
				curr = head;
				prev = NULL;

				if (position == 0) // deleting first node in a list
				{
					prev = curr;
					curr = curr->next;
					head = curr;
					prev = nullptr;
				}
				else if (position > 0 && position < sizeOfList) // deleting a node that is not the head of a list
				{
					for (int i = 0; i < position; i++)
					{
						prev = curr;
						curr = curr->next;
					}

					prev->next = curr->next;

					if (curr->next == NULL) // update tail if erasing the last node
						tail = prev;

					curr = nullptr;
				}

				sizeOfList -= 1; 
			}
		}

		void clear() // clear the LinkedList to not contain any nodes
		{
			curr = head;
			prev = NULL;

			for (int i = 0; i <= sizeOfList; i++)
			{
				prev = curr;
				curr = curr->next;
				prev = nullptr;
				sizeOfList -= 1; 
			}

			head = NULL;
			tail = NULL;
		}

		T& operator[] (int index) // retrieve a reference to data in a specific node
		{
			curr = head;
			for (int i = 0; i < index; i++)
			{
				curr = curr->next;
			}
			return *(curr->data);
		}

		int size() // retrieve how many nodes are in a list
		{
			return sizeOfList;
		}
	};
}