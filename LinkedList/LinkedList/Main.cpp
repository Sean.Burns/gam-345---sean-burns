// Main.cpp
// This file contains the main function that tests the LinkedList class.
// Sean Burns

#include <iostream>
#include "GAM345LinkedList.h"

//Main function
int main(int argc, char **argv)
{
	// declaration of test variables
	int a = 10;
	int b = 20;
	int c = 30;

	int* pA = &a;
	int* pB = &b;
	int* pC = &c;

	// initialize an empty linked list
	GAM345::LinkedList<int> testLinkedList;

	// test the size of the empty LinkedList (should print a size of 0)
	std::cout << "size of empty list = " << testLinkedList.size() << '\n'; 

	// insert 'a' into the list as the first node
	testLinkedList.insert(pA, 0);
	
	// test the size of the list (should print a size of 1)
	std::cout << "size of list after the first insert = " << testLinkedList.size() << '\n';

	//print data within list (should print data : 10)
	for (int i = 0; i < testLinkedList.size(); i++)
	{
		std::cout << "testLinkedList[" << i << "] = " << testLinkedList[i] << '\n';
	}

	// insert 'c' in the first position of the list
	testLinkedList.insert(pC, 0);
	// insert the third element 'b' in the last position of the list
	testLinkedList.insert(pB, testLinkedList.size());
	// insert the fourth element 'c' in the third position of the list
	testLinkedList.insert(pC, 2);

	// test the size of the list (should print a size of 4)
	std::cout << "size of list after inserts = " << testLinkedList.size() << '\n';

	//print data within list (should print data : 30,10,30,20)
	for (int i = 0; i < testLinkedList.size(); i++)
	{
		std::cout << "testLinkedList[" << i << "] = " << testLinkedList[i] << '\n';
	}

	// erase the first node of the list
	testLinkedList.erase(0);
	// erase the second node of the list
	testLinkedList.erase(1);
	// erase the last node of the list
	testLinkedList.erase(testLinkedList.size());

	// test the size of the list (should print a size of 1)
	std::cout << "size of list after erases = " << testLinkedList.size() << '\n';

	//print data within list (should print data : 10)
	for (int i = 0; i < testLinkedList.size(); i++)
	{
		std::cout << "testLinkedList[" << i << "] = " << testLinkedList[i] << '\n';
	}

	// clear the list
	testLinkedList.clear();

	// test the size of the list (should print a size of 0)
	std::cout << "size of list after clear = " << testLinkedList.size() << '\n';

	return 0;
}