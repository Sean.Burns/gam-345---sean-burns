// Gam345Queue.h
// This file contains the queue data structure class.
// Sean Burns

#pragma once

namespace GAM345
{

	template <class T>

	class Queue
	{

	private:
		class node // Node class in a Queue.
		{
		public:
			T* data; // Data within a node.
			node* next; // Pointer to the next node in the queue. 
		};
		node* head; // Pointer to the first node in the queue.
		node* tail; // Pointer to the last node in the queue.
		node* curr; // Pointer to the current node in the queue.
		node* prev; // Pointer used to change previous nodes' pointers.
		int sizeOfQueue; // The number of nodes in the queue.

	public:
		// construct an empty queue
		Queue() 
		{
			head = nullptr;
			tail = nullptr;
			sizeOfQueue = 0;
		}

		// insert a node into a queue in the last position
		void push(T* newElement) 
		{
			// creating a node
			node* n = new node;
			n->data = reinterpret_cast<T*>(new char[sizeof(T)]);
			n->data = newElement;

			if (head == nullptr) // placing the first node in a queue
			{
				head = n;
				tail = n;
				n->next = nullptr;
			}
			else if (sizeOfQueue > 0) // placing a node in a queue
			{
				prev = tail;
				prev->next = n;
				tail = n;
				n->next = nullptr;
			}

			// increase size of the queue
			sizeOfQueue += 1;
		}

		// removes and returns the first node from the queue
		T* pop() 
		{
			// set curr and prev to naviagte queue
			curr = head;
			prev = nullptr;

			// create node and set equal to the head node
			node* temp = new node;
			temp->data = reinterpret_cast<T*>(new char[sizeof(T)]);
			temp = curr;

			// remove the head node and reassign the head to the next node
			prev = curr;
			curr = curr->next;
			head = curr;
			prev = nullptr;

			// decrement the size of the queue
			sizeOfQueue -= 1;

			// return the first node
			return temp->data;
		}			

		// returns how many nodes are in a queue
		int size() 
		{
			return sizeOfQueue;
		}
	};
}

