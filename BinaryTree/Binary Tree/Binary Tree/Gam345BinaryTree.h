// Gam345BinaryTree.h
// This file contains the binary tree data structure class.
// Sean Burns

#pragma once
#include "Gam345Queue.h"
#include <iostream>

namespace GAM345
{

	class BinaryTree
	{
	private:
		// struct for a node within a binary tree
		struct BTreeNode
		{
			int value;
			BTreeNode* left;
			BTreeNode* right;
		};

		// initialize needed variables (nodes, int)
		BTreeNode* mRoot;
		BTreeNode* curr;
		BTreeNode* prev;
		int sizeOfTree;

		// inserts a node into a tree
		void insertNode(int aValue, BTreeNode* nodePtr)
		{
			BTreeNode* n = new BTreeNode;

			if (mRoot == nullptr) // create the first node
			{
				// add node
				n->value = aValue;
				n->left = nullptr;
				n->right = nullptr;

				sizeOfTree += 1; // increase size of tree
				mRoot = n; // set root to node
			} 
			else if (aValue < nodePtr->value) // move down left branch
			{
				if (nodePtr->left != nullptr)
				{
					insertNode(aValue, nodePtr->left); // recursion
				}
				else
				{
					// add node
					nodePtr->left = n;
					n->value = aValue;
					n->left = nullptr;
					n->right = nullptr;

					sizeOfTree += 1; // increase size of tree
				}
			}
			else if (aValue > nodePtr->value) // move down right branch
			{
				if (nodePtr->right != nullptr)
				{
					insertNode(aValue, nodePtr->right); // recursion
				}
				else
				{
					// add node
					nodePtr->right = n;
					n->value = aValue;
					n->left = nullptr;
					n->right = nullptr;

					sizeOfTree += 1; // increase size of tree
				}
			}
			else // value is already in tree
			{
				// do nothing
			}
		}

		// print all elements of the tree in order
		void PrintInOrder(BTreeNode* nodePtr)
		{
			// in-order traversal 
			if (mRoot != nullptr)
			{
				if (nodePtr->left != nullptr) // move down left branch
				{
					PrintInOrder(nodePtr->left); // recursion
				}
				std::cout << nodePtr->value << " "; // print data in node
				if (nodePtr->right != nullptr) // move down right branch
				{
					PrintInOrder(nodePtr->right); // recursion
				}
			}
			else if (mRoot == nullptr) // tree is empty
			{
				// do nothing
			}
		}

		// clear the binary tree
		void Clear(BTreeNode*& nodePtr)
		{
			// post-order traversal
			if (nodePtr != nullptr)
			{
				if (nodePtr->left != nullptr) // move down left branch
				{
					Clear(nodePtr->left); // recursion
				}

				if (nodePtr->right != nullptr) // move down right branch
				{
					Clear(nodePtr->right); // recursion
				}

				// delete node pointer and set null
				delete nodePtr;
				nodePtr = nullptr;

				// set the size to zero
				sizeOfTree = 0;
			}
		}

	public:

		// construct an empty binary tree
		BinaryTree()
		{
			mRoot = nullptr;
			sizeOfTree = 0;
		}

		// pass root node into recursive clear function
		~BinaryTree()
		{
			Clear(mRoot);
		}

		// passes the root into a recursive insert function
		void Insert(int aValue)
		{
			insertNode(aValue, mRoot);
		}

		// uses Breadth First Search to determine if the tree contains a specific element
		bool Contains(int aValue)
		{
			Queue <BTreeNode> nextNode;
			BTreeNode* n = new BTreeNode;
			nextNode.push(mRoot);

			while (nextNode.size() != 0)
			{
				n = nextNode.pop(); // pop first nnode from queue

				if (n->value == aValue) // check if data is aValue
				{
					return true;
				}

				if (n->left != nullptr)
				{
					nextNode.push(n->left); // push left node into queue
				}

				if (n->right != nullptr)
				{
					nextNode.push(n->right); // pushright node into queue
				}
			}

			return false;
		}

		// passes the root into a recursive print function
		void InOrderPrint()
		{
			PrintInOrder(mRoot);
		}

	};
}