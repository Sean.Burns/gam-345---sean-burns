// Main.cpp
// This file contains the main function that tests the functionality of the binary tree class.
// Sean Burns

#include <iostream>
#include "Gam345BinaryTree.h"

int main(int arggc, char** argv)
{
	// create empty binary tree
	GAM345::BinaryTree testTree;

	// insert data into the tree
	testTree.Insert(10);
	testTree.Insert(3);
	testTree.Insert(2);
	testTree.Insert(5);
	testTree.Insert(13);
	testTree.Insert(12);
	testTree.Insert(15);

	// print elements of the tree in order
	std::cout << "In-Order Print: ";
	testTree.InOrderPrint();

	// test true case for contains function
	if (testTree.Contains(3))
	{
		std::cout << '\n' << "Print this if the tree CONTAINS 3";
	}

	// test false case for contains function
	if (!testTree.Contains(20))
	{
		std::cout << '\n' << "Print this if the tree DOES NOT CONTAIN 20" << '\n';
	}

	// deconstruct binary tree
	testTree.~BinaryTree();

	// will result in no printing if tree was deconstructed
	testTree.InOrderPrint();

	return 0;
}