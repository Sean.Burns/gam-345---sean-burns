// Gam345Vector.h
// This file contains the vector class
// Sean Burns
#pragma once

namespace GAM345
{

	class Vector
	{

	private:
		int *data; // Pointer to data in vector 
		int sizeOfData; // How many elements are in vector
		int sizeOfCapacity; // How many elements the vector can hold
		bool duplicates; // Bool if a vector contains any duplicates found through partition function

		// quickSort function
		void Qsort(int* a, int low, int high)
		{
			// base case 
			if (low < high)
			{
				int partIdx = partition(a, low, high); // find the elements correct position
				Qsort(a, partIdx + 1, high); // right quicksort
				Qsort(a, low, partIdx - 1); // left quicksort
			}
		}

		// searches for a specific element in a vector
		bool search(int *a, int element)
		{
			int low = 0;
			int high = sizeOfData;

			while (low <= high)
			{
				int mid = (low + high) / 2;

				if (a[mid] == element)
				{
					return true;
				}
				else if (element < a[mid])
				{
					high = mid - 1;
				}
				else if (element > a[mid])
				{
					low = mid + 1;
				}
			}

			return false;
		}

	public:
		// Constructs an empty vector
		Vector()
		{
			data = reinterpret_cast<int*>(new char[sizeof(int) * 10]);
			sizeOfData = 0;
			sizeOfCapacity = 10;
			duplicates = false;
		}

		// Deconstructs a vector
		~Vector()
		{
			delete[] data;
			data = nullptr;
		}

		// Returns how many elements are in vector
		int size()
		{
			return sizeOfData;
		}

		// Returns how many elemnets can be in a vector
		int capacity()
		{
			return sizeOfCapacity;
		}

		// Returns the data held in a certain position in a vector
		int& operator[](int index)
		{
			return *(data + index);
		}

		// Puts an element into the vector
		void push_back(int newElement)
		{
			// Resizes vector if it is full
			if (sizeOfData + 1 > sizeOfCapacity)
			{
				// make a new temp vector
				int *temp = reinterpret_cast<int*>(new char[sizeof(int) * (sizeOfCapacity * 2)]);

				// copy data into new vector
				for (int i = 0; i < sizeOfData; i++)
				{
					temp[i] = data[i];
				}

				// delete starting vector
				delete[] data;

				// make starting vector equal to temp vector
				data = temp;

				// update capacity
				sizeOfCapacity = sizeOfCapacity * 2;
			}

			// insterts data into position
			data[sizeOfData] = newElement;

			// increment size of vector
			sizeOfData += 1;

		}

		// Swaps two elements in a vector
		void swap(int position1, int position2)
		{
			int temp = 0;

			temp = data[position1];
			data[position1] = data[position2];
			data[position2] = temp;
		}

		// reserves space for a vector
		void reserve(int newSizeOfCapacity)
		{
			// make a temp vector
			int *temp = reinterpret_cast<int*>(new char[sizeof(int) * newSizeOfCapacity + sizeOfCapacity]);

			// copy data from vector into temp vector
			for (int i = 0; i < sizeOfData; i++)
			{
				temp[i] = data[i];
			}

			// delete starting vector
			delete[] data;

			// make starting vector equal to temp vector
			data = temp;

			// set new capacity
			sizeOfCapacity = newSizeOfCapacity + sizeOfCapacity;
		}

		// public quickSort function
		void quickSort()
		{
			Qsort(data, 0, sizeOfData - 1);
		}

		// partition function
		int partition(int*a, int low, int high)
		{
			int mid = (low + high) / 2;
			int pivot = high;
			swap(pivot, mid);
			pivot = a[high];

			// select pivot index
			int partIdx = low;
			int temp = 0;

			// loop through vector comparing current element with the pivot
			for (int i = low; i < high; i++)
			{
				// check for duplicates
				if (a[i] == pivot)
				{
					duplicates = true; 
				}

				if (a[i] <= pivot)
				{
					swap(i, partIdx);
					partIdx++;
				}

			}

			swap(partIdx, high);
			return partIdx;
			
		}

		// calls and returns the private search function
		bool contains(int element)
		{
			return search(data, element);
		}

		// returns duplicates bool
		bool duplicate()
		{
			return duplicates;
		}
	};
}