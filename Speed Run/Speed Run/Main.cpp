// basic file operations
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "Gam345Vector.h"

using namespace std;

// Convert a string to a number
int convertToNumber(string s)
{
	// Convert the string into a character array
	const char* charArray = s.c_str();
	// Convert the char array into a number
	int number = atoi(charArray);
	return number;
}

int main() 
{

	string line;
	ifstream myfile("Numbers.txt");
	if (!myfile.is_open())
	{
		cout << "Unable to open Numbers.txt";
		return 0;
	}

	// Read how many numbers there are
	getline(myfile, line);
	int numberCount = convertToNumber(line);

	// Reserve the space in the vector
	GAM345::Vector numbers;
	numbers.reserve(numberCount);

	// Read the numbers into the vector
	getline(myfile, line); // Read the whole line
	stringstream ss(line);
	while (getline(ss, line, ' ')) {
		// Add the number to the vector
		numbers.push_back(convertToNumber(line));
	}

	// Finally, read the value to search for
	getline(myfile, line);
	int numberToFind = convertToNumber(line);

	// Close the file
	myfile.close();

	// Sort the vector
	numbers.quickSort();

	// See if the numberToFind is in the set of numbers
	if (numbers.contains(numberToFind))
	{
		cout << "True" << endl;
	}
	else if (!numbers.contains(numberToFind))
	{
		cout << "False" << endl;
	}
	
	// write to console if there are any duplicates
	if (numbers.duplicate())
	{
		cout << "True" << endl;
	}
	else if (!numbers.duplicate())
	{
		cout << "False" << endl;
	}

	return 0;
}