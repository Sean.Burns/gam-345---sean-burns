#pragma once

namespace GAM345
{

	template <class T>

	class Vector
	{

	private:
		T* data;
		int sizeOfData;
		int sizeOfCapacity;

	public:
		Vector() 
		{
			data = reinterpret_cast<T*>(new char[sizeof(T) * 10]);
			sizeOfData = 0;
			sizeOfCapacity = 10;
			for (int i = 0; i < sizeOfData+1; i++)
			{
				push_back(T());
			}
		}
		
		~Vector() 
		{
			delete[] data;
			data = nullptr;
		}

		void resize(int newSizeOfData)
		{
			if (newSizeOfData < sizeOfData)
			{
				T *temp = reinterpret_cast<T*>(new char[sizeof(T) * newSizeOfData * 2]);
				for (int i = 0; i < newSizeOfData; i++)
				{
					temp[i] = data[i];
				}

				delete[] data;
				data = temp;
				sizeOfData = newSizeOfData;
				sizeOfCapacity = newSizeOfData * 2;
			}
			else if (newSizeOfData > sizeOfCapacity)
			{
				T *temp = reinterpret_cast<T*>(new char[sizeof(T) * newSizeOfData * 2]);
				for (int i = 0; i < sizeOfData; i++)
				{
					temp[i] = data[i];
				}

				delete[] data;
				data = temp;
				sizeOfData = newSizeOfData;
				sizeOfCapacity = newSizeOfData * 2;
			}
			else if (newSizeOfData > sizeOfData && newSizeOfData < sizeOfCapacity)
			{
				sizeOfData = newSizeOfData;
			}
		}

		void reserve(int newSizeOfCapacity)
		{
				T *temp = reinterpret_cast<T*>(new char[sizeof(T) * newSizeOfCapacity + sizeOfCapacity]);
				for (int i  = 0; i < sizeOfData; i++) 
				{
					temp[i] = data[i];
				}

				delete[] data;
				data = temp;
				sizeOfCapacity = newSizeOfCapacity + sizeOfCapacity;
		}

		void push_back(T newElement)
		{
			if (sizeOfData + 1 > sizeOfCapacity)
			{
				T* temp = reinterpret_cast<T*>(new char[sizeof(T) * (sizeOfCapacity * 2)]);
				memcpy(temp, data, sizeof(T) * sizeOfData);
				delete[] data;
				data = temp;
				sizeOfCapacity = sizeOfCapacity * 2;
			}
			else
			{
				data[sizeOfData] = newElement;
				sizeOfData += 1;
			}
		}

		void remove(int position)
		{
			T* temp = reinterpret_cast<T*>(new char[sizeof(T) * (sizeOfCapacity)]);
			for (int i = 0; i < sizeOfData; i++)
			{
				if (i < position)
				{
					temp[i] = data[i];
				}
				else if (i > position)
				{
					temp[i - 1] = data[i];
				}
			}
			delete[] data;
			data = temp;
			sizeOfData = sizeOfData - 1;
		}

		void clear()
		{
			T* temp = reinterpret_cast<T*>(new char[sizeof(T) * (sizeOfCapacity)]);
			delete[] data;
			data = temp;
			sizeOfData = 0;
		}

		void insert(T newElement, int position)
		{
			if (position > sizeOfData + 1)
			{
				return;
			}
			else 
			{ 
				data[position] = newElement;
			}
		}

		bool empty()
		{
			if (sizeOfData == 0)
			{
				return true;
			}
			else if (sizeOfData != 0)
			{
				return false;
			}
		}

		int size()
		{
			return sizeOfData;
		}

		int capacity()
		{
			return sizeOfCapacity;
		}

		T& operator[](int index)
		{
			return *(data + index);
		}
	};
}
	


