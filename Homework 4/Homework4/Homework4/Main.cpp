// Main.cpp
// This file contains the insertionSort, mergeSort, merge, and main function
// Sean Burns

#include <iostream>

// insertionSort function 
void insertionSort(int *a, int size)
{
	int temp = 0; 
	int index = 0;

	for (int i = 0; i < size; i++) // loop through each element of array to compare
	{
		index = i; // reset index
		while (index > 0 && a[index - 1] > a[index]) // compare element of array at position index to the position before
		{
			// swap positions if position index-1 is larger than position index 
			temp = a[index];
			a[index] = a[index - 1];
			a[index - 1] = temp;
			index -= 1; // subtract index by one to compare the element at position index with new index-1
		}
	}
}

// merge function
void merge(int *left, int leftSize, int *right, int rightSize)
{
	// declare variables for merge function 
	int size = leftSize + rightSize;
	int leftIndex = 0;
	int rightIndex = 0;
	int arrayIndex = 0;
	
	// create temp array to merge into
	int mergeArray[10]; 

	// merge two sides placing elements in correct order into the temp array
	while (leftIndex != leftSize && rightIndex != rightSize)
	{

		if (left[leftIndex] > right[rightIndex])
		{
			mergeArray[arrayIndex] = right[rightIndex];
			arrayIndex += 1;
			rightIndex += 1;
		}
		else if (left[leftIndex] < right[rightIndex])
		{
			mergeArray[arrayIndex] = left[leftIndex];
			arrayIndex += 1;
			leftIndex += 1;
		}
	}

	// add any remaining elements on the left side
	while (leftIndex < leftSize)
	{
		mergeArray[arrayIndex] = left[leftIndex];
		arrayIndex += 1;
		leftIndex += 1;
	}

	// add any remaining elements on the right side
	while (rightIndex < rightSize)
	{
		mergeArray[arrayIndex] = right[rightIndex];
		arrayIndex += 1;
		rightIndex += 1;
	}
	
	// convert array temp array into original array
	for (int i = 0; i < size; i++)
	{
		left[i] = mergeArray[i];
	}
}
	


// mergeSort function
void mergeSort(int *a, int size)
{
	// declare midpoint and rightSize variable
	int mid = (size / 2);
	int rightSize = size - mid;

	if (size == 2) // compare &swap if necessary if array is only two elements 
	{
		int temp = 0;
		int index = 1;
		if (a[index - 1] > a[index])
		{
			temp = a[index];
			a[index] = a[index - 1];
			a[index - 1] = temp;
		}
	}
	else if (size == 1) // do nothing if the size is equal to 1
	{

	}
	else // recursion if the size does not equal 2 or 1
	{
		mergeSort(a, mid); // recursion on left side
		mergeSort(a + mid, rightSize); // recursion on right side
		merge(a, mid, a + mid, rightSize);
	}
}

// Main function
int main(int argc, char **argv)
{
	// create unsorted arrays to test sorting functions
	int testArray[10] = { 2, 1, 25, 10, 30, 36, 100, 57, 70, 15 };
	int testArray2[10] = { 2, 1, 25, 10, 30, 36, 100, 57, 70, 15 };

	int size = sizeof(testArray) / sizeof(*testArray); // size of the first array
	int size2 = sizeof(testArray2) / sizeof(*testArray2); // size of the second array

	// write unsorted testArray to console
	for (int i = 0; i < size; i++)
	{
		if (i == 0)
			std::cout << "Unsorted array: " << testArray[i] << " ";
		else if (i == size - 1)
			std::cout << testArray[i] << '\n';
		else
			std::cout << testArray[i] << " " ;
	}

	// sort array using insertionSort function
	insertionSort(testArray, size);

	// write sorted testArray to console
	for (int i = 0; i < size; i++)
	{
		if (i == 0)
			std::cout << "insertionSort: " << testArray[i] << " ";
		else if (i == size - 1)
			std::cout << testArray[i] << '\n';
		else
			std::cout << testArray[i] << " ";
	}

	// write unsorted testArray2 to console
	for (int i = 0; i < size2; i++)
	{
		if (i == 0)
			std::cout << "Unsorted array: " << testArray2[i] << " ";
		else if (i == size2 - 1)
			std::cout << testArray2[i] << '\n';
		else
			std::cout << testArray2[i] << " ";
	}

	// sort array using mergeSort function
	mergeSort(testArray2, size2);

	// write sorted testArray2 to console
	for (int i = 0; i < size2; i++)
	{
		if (i == 0)
			std::cout << "mergeSort: " << testArray2[i] << " ";
		else if (i == size2 - 1)
			std::cout << testArray2[i] << '\n';
		else
			std::cout << testArray2[i] << " ";
	}

	return 0;
}

