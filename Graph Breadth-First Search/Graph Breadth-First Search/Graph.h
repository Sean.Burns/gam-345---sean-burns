#pragma once
#include <vector>
#include "Node.h"

// This class represents a basic graph data structure
class Graph
{
public:

	Graph();
	~Graph();

	// Add a new node to the graph
	void AddNode(char aNodeId, int aNumConnections, const char* aConnectedNodeList);

	// Get the node with the given id
	bool GetNode(char aNodeId, Node& aNode);

	// Find a path from the start node to the end node
	char* FindPath(const char aStartId, const char aEndID);

private:
	// A container that holds all of the nodes within the graph
	std::vector<Node> nodes;
};
