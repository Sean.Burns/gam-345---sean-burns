#include <iostream>
#include "Graph.h"

int main(int argc, char** argv)
{
	// Create the graph
	Graph g;
	g.AddNode('A', 2, "DB");
	g.AddNode('B', 4, "ADCH");
	g.AddNode('C', 2, "BF");
	g.AddNode('D', 3, "ABE");
	g.AddNode('E', 2, "DG");
	g.AddNode('F', 3, "CGI");
	g.AddNode('G', 2, "EF");
	g.AddNode('H', 2, "BI");
	g.AddNode('I', 2, "FH");

	// Find a path from node A to G
	char* path = g.FindPath('A', 'G');
	std::cout << path << std::endl;
	delete path;

	// Find a path from node A to I
	path = g.FindPath('A', 'I');
	std::cout << path << std::endl;
	delete path;

	// Find a path from node H to E
	path = g.FindPath('H', 'E');
	std::cout << path << std::endl;
	delete path;

	// Find a path from node C to B
	path = g.FindPath('C', 'B');
	std::cout << path << std::endl;
	delete path;

	// Find a path from node G to C
	path = g.FindPath('G', 'C');
	std::cout << path << std::endl;
	delete path;

	// Find a path from node A to Z
	path = g.FindPath('A', 'Z');
	std::cout << path << std::endl;
	delete path;
}