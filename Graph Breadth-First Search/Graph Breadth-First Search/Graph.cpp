#include "Graph.h"
#include <vector>
#include <queue>
#include <map>

Graph::Graph()
{
}

Graph::~Graph()
{
}

void Graph::AddNode(char aNodeId, int aNumConnections, const char* aConnectedNodeList)
{
	nodes.push_back(Node(aNodeId, aNumConnections, aConnectedNodeList));
}

bool Graph::GetNode(char aNodeId, Node& aNode)
{
	for (int i = 0; i < nodes.size(); i++)
	{
		if (nodes[i].nodeId == aNodeId)
		{
			aNode = nodes[i];
			return true;

		}
	}
	return false;
}

char* Graph::FindPath(const char aStartId, const char aEndID)
{
	// Create the frontier, add aStartId to it
	std::queue<char> frontier;
	frontier.push(aStartId);
	
	// Created the visited list, add aStartId to it
	std::vector<char> visited;
	visited.push_back(aStartId);

	// Store path information
	std::map<char, char> cameFrom;
	cameFrom[aStartId] = '\0'; // Add that the start node came from a null character

	// NOTES:
	//frontier.front() // Gets the element from the front of the queue without removing it
	//frontier.pop(); // Removes the element at the front of the queue
	
	// To get a node using the above functions:	
	//Node n;
	//bool found = GetNode('A', n);

	// TODO: Your code for Breadth-First Search here

	// loops until queue is empty
	while (!frontier.empty())
	{
		// get element from beggining of queue
		Node curr;
		bool found = GetNode(frontier.front(), curr);
		frontier.pop();

		// loop through each connection 
		for(int i = 0; i < curr.numConnections; i++)
		{
			//bool to checck if connection has already been checked
			bool contains = false;

			// loop through visited vector
			for (int j = 0; j < visited.size(); j++)
			{
				// check if element has already been visited
				if (visited[j] == curr.connections[i])
				{
					contains = true; // vector contains the connection
				}
			}

			if (!contains) // add connection if visited does not contain it
			{
				frontier.push(curr.connections[i]); // add connection in frontier
				visited.push_back(curr.connections[i]); // add connection in visited
				cameFrom[curr.connections[i]] = curr.nodeId; // add the connection and where it came from into cameFrom map
			}
		}
	}

	// CREATE PATH

	// initialize needed variables
	bool pathFound = false;
	char index = aEndID;
	char* returnPath;
	char path[] = { '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0' };
	int itr = 0;

	// set first position in path to the endID
	path[itr] = index;

	// loop until path is found
	while (!pathFound)
	{
		// increase iterator
		itr += 1;

		
		index = cameFrom.find(index)->second; // set index to the element that added it to the map
		path[itr] = index; // set next position in the path to index

		// if the current element is the startingID end loop
		if (cameFrom.find(index)->second == NULL)
		{
			pathFound = true;
		}
	}

	// reverse the path so it reads: s->e
	for (int i = 0; i < itr; i++, itr--)
	{
		char temp = path[i];
		path[i] = path[itr];
		path[itr] = temp;
	}

	// make the return pointer equal to the path
	returnPath = path;

	if (path[0] == '\0')
	{
		return nullptr; // if the path does not exisit return null
	}
	else
	{
		return returnPath; //return path
	}
}

