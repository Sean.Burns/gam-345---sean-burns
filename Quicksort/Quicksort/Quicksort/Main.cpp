// Sean Burns
// Main function

#include <iostream>
#include "Gam345Vector.h"
#include <Windows.h>

// main function 
int main(int argc, char **argv)
{
	// initialize ints for timer
	unsigned long long int quickStartTime;
	unsigned long long int quickEndTime;
	unsigned long long int mergeStartTime;
	unsigned long long int mergeEndTime;

	
	std::cout << '\n' << '\n' << "TESTS WITH RANDOM ELEMENTS 0-100" << '\n';

	// test loop
	for (int i = 100; i < 40001; i)
	{
		// initialize test vectors
		GAM345::Vector quickVector;
		GAM345::Vector mergeVector;

		// fill vectors with 'i' amount of random ints
		for (int k = 0; k < i; k++)
		{
			quickVector.push_back(rand() % 100);
			mergeVector.push_back(rand() % 100);
		}

		// time quick sort
		quickStartTime = GetTickCount64();
		quickVector.quickSort();
		quickEndTime = GetTickCount64();

		// time merge sort
		mergeStartTime = GetTickCount64();
		mergeVector.mergeSort();
		mergeEndTime = GetTickCount64();

		// write data to console
		std::cout << i << "ELEMENTS" << '\n';
		std::cout << "QUICK SORT - " << quickEndTime - quickStartTime << " milliseconds." << '\n';
		std::cout << "MERGE SORT - " << mergeEndTime - mergeStartTime << " milliseconds." << '\n';
		std::cout << '\n';

		// destroy test vectors
		quickVector.~Vector();
		mergeVector.~Vector();

		// increment i
		if (i == 100)
		{
			i += 400;
		}
		else
		{
			i += 500;
		}
	}

	return 0;
}