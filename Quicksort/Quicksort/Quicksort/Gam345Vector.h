// Sean Burns
// Vector class
// Re-wrote vector class for ints with necessary functions
#pragma once

namespace GAM345
{

	class Vector
	{

	private:
		int *data; // Pointer to data in vector 
		int sizeOfData; // How many elements are in vector
		int sizeOfCapacity; // How many elements the vector can hold

		// quickSort function
		void Qsort(int* a, int low, int high)
		{
			// base case 
			if (low < high)
			{
				int partIdx = partition(a, low, high); // find the elements correct position
				Qsort(a, partIdx + 1, high); // right quicksort
				Qsort(a, low, partIdx - 1); // left quicksort
			}
		}

		// mergeSort function
		void Msort(int *a, int start, int end)
		{
			// declare midpoint and rightSize variable
			int mid = (start + end) / 2;

			// base case if the size is 1
			if (start >= end)  
			{
				return;
			}
			
			// recursion if the size does not equal 1
			Msort(a, start, mid); // recursion on left side
			Msort(a, mid + 1, end); // recursion on right side
			merge(a, start, end); // merge two sections back together
			
		}


		


	public:
		// Constructs an empty vector
		Vector()
			{
			data = reinterpret_cast<int*>(new char[sizeof(int) * 10]);
			sizeOfData = 0;
			sizeOfCapacity = 10;
		}

		// Deconstructs a vector
		~Vector()
		{
			delete[] data;
			data = nullptr;
		}

		// Returns how many elements are in vector
		int size()
		{
			return sizeOfData;
		}

		// Returns how many elemnets can be in a vector
		int capacity()
		{
			return sizeOfCapacity;
		}

		// Returns the data held in a certain position in a vector
		int& operator[](int index)
		{
			return *(data + index);
		}

		// Puts an element into the vector
		void push_back(int newElement)
		{
			// Resizes vector if it is full
			if (sizeOfData + 1 > sizeOfCapacity)
			{
				// make a new temp vector
				int *temp = reinterpret_cast<int*>(new char[sizeof(int) * (sizeOfCapacity * 2)]);

				// copy data into new vector
				for (int i = 0; i < sizeOfData; i++)
				{
					temp[i] = data[i];
				}

				// delete starting vector
				delete[] data;

				// make starting vector equal to temp vector
				data = temp;

				// update capacity
				sizeOfCapacity = sizeOfCapacity * 2;
			}

			// insterts data into position
			data[sizeOfData] = newElement;

			// increment size of vector
			sizeOfData += 1;

		}

		// Swaps two elements in a vector
		void swap(int position1, int position2)
		{
			int temp = 0;

			temp = data[position1];
			data[position1] = data[position2];
			data[position2] = temp;
		}

		// reserves space for a vector
		void reserve(int newSizeOfCapacity)
		{
			// make a temp vector
			int *temp = reinterpret_cast<int*>(new char[sizeof(int) * newSizeOfCapacity + sizeOfCapacity]);

			// copy data from vector into temp vector
			for (int i = 0; i < sizeOfData; i++)
			{
				temp[i] = data[i];
			}

			// delete starting vector
			delete[] data;

			// make starting vector equal to temp vector
			data = temp;

			// set new capacity
			sizeOfCapacity = newSizeOfCapacity + sizeOfCapacity;
		}

		// public quickSort function
		void quickSort()
		{
			Qsort(data, 0, sizeOfData - 1);
		}

		// partition function
		int partition(int*a, int low, int high)
		{
			int pivot = a[high];

			// select pivot index
			int partIdx = low;
			int temp = 0;

			// loop through vector comparing current element with the pivot
			for (int i = low; i < high; i++)
			{

				if (a[i] <= pivot)
				{
					swap(i, partIdx);
					partIdx++;
				}

			}

			swap(partIdx, high);
			return partIdx;
		}

		// mergeSort function
		void mergeSort()
		{
			Msort(data, 0, sizeOfData - 1);
		}

		// merges two vectors into one vector
		void merge(int *a, int start, int end)
		{
			// declare variables for merge function 
			int mid = (start + end) / 2;
			int leftIdx = start;
			int rightIdx = mid + 1;
			int vecIdx = start;

			// create temp array to merge into
			Vector mergeVec;
			mergeVec.reserve(start + end);

			// loop through vector
			while (leftIdx <= mid && rightIdx <= end)
			{
				// compare elements
				if (a[leftIdx] < a[rightIdx])
				{
					mergeVec[vecIdx++] = a[leftIdx++];
				}
				else
				{
					mergeVec[vecIdx++] = a[rightIdx++];
				}
			}
			// add remaining left elements
			while (leftIdx <= mid)
			{
				mergeVec[vecIdx++] = a[leftIdx++];
			}
			// add remaining right elements
			while (rightIdx <= end)
			{
				mergeVec[vecIdx++] = a[rightIdx++];
			}

			// copy data back into starting vector
			for (int i = start; i <= end; i++)
			{
				a[i] = mergeVec[i];
			}	
		}
	};
}
